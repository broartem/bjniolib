#include "bjn.h"

/* TODO 64 bit platform portability */
static long a_l_lmem,m_e_m;

#define allocate_memory_n1(type,name,defper)                       \
 type * name(int n)                                                  \
{                                                                  \
 defper x_tmp;                                                     \
 char far *x_cha;                                                  \
 int  i;                                                           \
 m_e_m=(long)n*(long)sizeof(type);                                   \
if(0 && m_e_m>128*1000*1000) {printf("\n Aborting allocation %d memory block > 16*1000*1000 \n",m_e_m); \
	 exit(0);} \
 a_l_lmem+=m_e_m;                                                      \
 if(bTestPrint) \
 printf(" n1 %6ld : %4ld K : %6ld K\n",m_e_m,m_e_m/1024L,a_l_lmem/1024L);\
 if(0 && (long)n*(long)sizeof(type)>64L*1024L)                          \
         {                                                         \
         printf(ErrAllocMsg1);                                     \
         exit(1);                                                  \
         }                                                         \
                                                                   \
 if( (x_tmp=(type*)_fmalloc(n*(long)sizeof(type))) == NULL)              \
         {                                                         \
         printf(ErrAllocMsg);                                      \
         exit(1);                                                  \
         }                                                         \
                                                                   \
  x_cha = (char far *)x_tmp;                                       \
                                                                   \
  for(i=0;i<n*(long)sizeof(type);i++) x_cha[i]='\000';                   \
                                                                   \
  return(x_tmp);                                                   \
}                                                                  \

/*----------------------------------------------------------------*/

#define allocate_memory_n1_n2(type,name)                           \
 type  **name(int n,int m)                                         \
{                                                                  \
 register int i,k;                                                 \
 type far **x_tmp;                                                 \
 char far *x_cha;                                                  \
                                                                   \
 m_e_m=(long)n*(long)sizeof(type*)+(long)n*(long)m*(long)sizeof(type*);\
 a_l_lmem+=m_e_m;                                                      \
 if(bTestPrint) \
 printf(" n2 %6ld : %4ld K : %4ld K\n",m_e_m,m_e_m/1024L,a_l_lmem/1024L);\
                                                                   \
 if(0 && ((long)n*(long)sizeof(type *)>64L*1024L))                        \
         {                                                         \
         printf(ErrAllocMsg2);                                     \
         exit(1);                                                  \
         }                                                         \
                                                                   \
 if(0 && (long)m*(long)sizeof(type  )>64L*1024L)                        \
         {                                                         \
         printf(ErrAllocMsg1);                                     \
         exit(1);                                                  \
         }                                                         \
                                                                   \
 if( (x_tmp=(type **)_fmalloc(n*sizeof(type *))) == NULL)          \
         {                                                         \
         printf(ErrAllocMsg);                                      \
         exit(1);                                                  \
         }                                                         \
                                                                   \
       if( (x_tmp[0]=(type *)_fmalloc(m*n*sizeof(type))) == NULL)  \
         {                                                         \
         printf(ErrAllocMsg);                                      \
         exit(1);                                                  \
         }                                                         \
																	\
  for(i=0; i < n; i++)                                             \
       {                                                           \
       x_tmp[i]=x_tmp[0]+i*m;    \
                                                                   \
       x_cha = (char far *)x_tmp[i];                               \
       for(k=0;k<m*(long)sizeof(type);k++) x_cha[k]='\000';              \
       }                                                           \
  return(x_tmp);                                                   \
}                                                                  \

/*----------------------------------------------------------------*/



allocate_memory_n1     (char far   , alloc_char_mas_n1     ,LPCHAR)
allocate_memory_n1     (int    , alloc_int_mas_n1          ,LPINT)
allocate_memory_n1     (float  , alloc_float_mas_n1        ,LPFLOAT)
allocate_memory_n1     (double , alloc_double_mas_n1       ,LPDOUBLE)

allocate_memory_n1_n2  (char   , alloc_char_mas_n1_n2)
allocate_memory_n1_n2  (int   , alloc_int_mas_n1_n2)
allocate_memory_n1_n2  (float  , alloc_float_mas_n1_n2)
allocate_memory_n1_n2  (double , alloc_double_mas_n1_n2)



/* TODO ��� ����������!
LPLPLPCHAR   alloc_char_mas_n1_n2_n3(int n1, int n2, int n3);
LPLPLPINT   alloc_int_mas_n1_n2_n3(int n1);
LPLPLPFLOAT  alloc_float_mas_n1_n2_n3(int n1, int n2, int n3);
LPLPLPDOUBLE alloc_double_mas_n1_n2_n3(int n1, int n2, int n3);

*/
/*
LPLPLPINT alloc_int_mas_n1_n2_n3(int n1, int n2, int n3)
{
 int i, j;
 LPLPLPINT p;
 LPINT p1;

 p = (LPLPLPINT)alloc_char_mas_n1(n1*sizeof(LPLPINT));
 for(i=0; i<n1; i++)
 p[i] = (LPLPINT)alloc_char_mas_n1(n2*sizeof(LPLPLPINT));
 
 p1 = alloc_int_mas_n1(n1*n2*n3);

 for(i=0; i<n1; i++)
 for(j=0; j<n2; j++)
 p[i][j] = &(p1[i*n2*n3 + j*n3]); 

 //for(i=0;i<n1;i++)
 //  p[i] = alloc_float_mas_n1_n2(n2,n3);

 return p;
}
*/

LPLPLPFLOAT alloc_float_mas_n1_n2_n3(int n1, int n2, int n3)
{
 int i, j;
 LPLPLPFLOAT p;
 LPFLOAT p1;

 p = (LPLPLPFLOAT)alloc_char_mas_n1(n1*sizeof(LPLPFLOAT));
 for(i=0; i<n1; i++)
 p[i] = (LPLPFLOAT)alloc_char_mas_n1(n2*sizeof(LPFLOAT));
 
 p1 = alloc_float_mas_n1(n1*n2*n3);

 for(i=0; i<n1; i++)
 for(j=0; j<n2; j++)
 p[i][j] = &(p1[i*n2*n3 + j*n3]); 

 //for(i=0;i<n1;i++)
 //  p[i] = alloc_float_mas_n1_n2(n2,n3);

 return p;
}

LPLPLPINT alloc_int_mas_n1_n2_n3(int n1, int n2, int n3)
{
 int i, j;
 LPLPLPINT p;
 LPINT p1;

 p = (LPLPLPINT)alloc_char_mas_n1(n1*sizeof(LPLPINT));
 for(i=0; i<n1; i++)
 p[i] = (LPLPINT)alloc_char_mas_n1(n2*sizeof(LPINT));
 
 p1 = alloc_int_mas_n1(n1*n2*n3);

 for(i=0; i<n1; i++)
 for(j=0; j<n2; j++)
 p[i][j] = &(p1[i*n2*n3 + j*n3]); 

 return p;
}


#define P (*p)

void free_mas_n1_n2(LPLPVOID *p,int n1,int n2)
{
// int i;

 if(bTestPrint)
 printf(" free_mas_n1_n2 : %d x %d \n",n1,n2);

//  for(i=0;i<1 /*n1*/;i++)
   _ffree(P[0]);

 _ffree(P);
  P = NULL;
}

void free_mas_n1_n2_n3(LPLPLPVOID *p,int n1,int n2,int n3)
{
  int i;

  free((*p)[0][0]);

  for(i=0; i<n1; i++) 
    free((*p)[i]);

  free((*p));

  return;
/*TODO HACK MEMORY DEADLOCK*/
 if(0)
 printf(" free_mas_n1_n2 : %d x %d \n",n1,n2);

  for(i=0;i<n1;i++)
	free_mas_n1_n2(&((*p)[i]),n2,n3);

 _ffree((*p));
  (*p) = NULL;
}

