#ifndef _BJNTYPES_H_
#define _BJNTYPES_H_

/* TODO: �������� ��������������� ������� ����� ��� ������������� ����� 32- � 64- ������� ������������� ���������. */
/* TODO: �������� ����������� ���� � ����,  ������������ ��� ���������� BJN */
#define far

//typedef void      * LPVOID;
//typedef char      * LPCHAR;
//typedef int       * LPINT;
//typedef float     * LPFLOAT;
typedef double    * LPDOUBLE;

//typedef LPVOID    * LPLPVOID;
//typedef LPCHAR    * LPLPCHAR;
typedef LPINT     * LPLPINT;
//typedef LPFLOAT   * LPLPFLOAT;
typedef LPDOUBLE  * LPLPDOUBLE;

//typedef LPLPVOID   * LPLPLPVOID;
//typedef LPLPCHAR   * LPLPLPCHAR;
//typedef LPLPINT    * LPLPLPINT;
//typedef LPLPFLOAT  * LPLPLPFLOAT;
typedef LPLPDOUBLE * LPLPLPDOUBLE;

#define MAXNAMELEN 1024

#define sizeof_file_offset	4
#define sizeof_big_size		4
#define sizeof_float		4

//							               1234567890123456
#define LABELScalar8RecGzip01 "BJN_3D_RC_GZ_01 "
#define LABELScalar8RecGzip03 "BJN_3D_RC_GZ_03 "


#ifndef ZLIB_H
	typedef unsigned long uLong;
	
#endif /* ZLIB_H */

typedef long	file_offset; 

int file_seek(FILE *stream, file_offset offset, int origin);

typedef struct
{
/*	big_size	n1,n2,n3;				// ����� ����� �� �����������
	file_offset	first_func_offset;	// �������� �� ���������� �������
	LPCHAR		tagOfBjnFile;			// ��� ���� �����
	LPCHAR		grid;					// ��� ����� �����
*/
} BJN_HEADER;

typedef BJN_HEADER *LPBJN_HEADER;

typedef struct
{
/*TODO ����������� � uLong  */
  file_offset	next_func_offset;	// �������� �� ������ ������� ������ �� ������ �����
  file_offset	first_block_offset;	// �������� ������ �� ������ ������� ������ � �� ������� 
  float			fmin,fmax;			// compression ratio
  LPCHAR		name;				// ��� �������
}	FUNC_HEADER;		// ������� ���������� �������

typedef FUNC_HEADER *LPFUNC_HEADER;

typedef struct
{
  file_offset	next_block_offset;	// ��������� ���������� ����� 
  big_size		i0,j0,k0;			// ������ ����� � ���������� ���������
  big_size		ni,nj,nk;			// ������� �����
  big_size		compressed_size;	// ������ ������� �����
} BLOCK_HEADER;

typedef BLOCK_HEADER *LPBLOCK_HEADER;

#endif /* _BJNTYPES_H_ */
