#include "bjn.h"
#include "string.h"

#define LPSTR LPCHAR
#define LPLPSTR LPLPCHAR

/*

int  ReadBjnGzippedScalar8RecInfo(LPCHAR source, LPBJN_INFO pbjn_info)
{
	return 0;
}

void FreeBjnInfo(LPBJN_INFO pbjn_info)
{
}

*/

int  ReadBjnGzippedScalar8RecInfo(LPSTR source, LPBJN_INFO pbjn_info)
{
 FILE		*fin;
 int		 err,i,bytes;
 file_offset pfunc,plast;
 file_offset pdata;

 if(!pbjn_info)
	 return 204;

  if(BjnDummy)
 {
	 static float fmin[1]= {0.f};
	 static float fmax[1]= {10.f};
	 static LPCHAR  (funcNames[2])={(char *)"memF1",(char *)""};

	 printf("BjnDummy INFO\n");

	 pbjn_info->n1=11;
	 pbjn_info->n2=11;
	 pbjn_info->n3=11;
	 pbjn_info->filename=(char *)"memory";
	 pbjn_info->n_func=1;
	 pbjn_info->gridname=(char *)"";
	 strncpy(pbjn_info->tagOfBjnFile,(char *)"mem",LLABEL);
	 pbjn_info->fmin=fmin;
	 pbjn_info->fmax=fmax;
	 pbjn_info->funcNames=funcNames;

	 return 0;
  }
		
		
	
 if( (fin=fopen(source,"rb")) == NULL )
   { printf("\n Can't open file %s ",source ); return 205;} 
 {

	 //char tagbuf[LLABEL+1];
	 char *tagbuf;
	 tagbuf = alloc_char_mas_n1 (LLABEL+1);
	 tagbuf[LLABEL] = '\0';
	//
	
	
	//printf("LLABEL = %d",LLABEL);
	
	 bytes=fread((LPCHAR)tagbuf,LLABEL,1,fin);
	 //printf("sorse is =  %s ",tagbuf );
	 

	 if(bytes != 1) { printf("\n Can't read tag of file %s\n",source ); return 206; }
	
	/*
	//����� �� �������� ���������, ����� ���� �� �������� �� ������� �����. 
	 if(strstr((char *)LABELScalar8RecGzip03,(char *)tagbuf))
		{ printf("\n Incorrect type %s of %s\n",tagbuf,source );		  
		  printf(" LABELScalar8RecGzip03 = %s \n",LABELScalar8RecGzip03);
		 printf("from bjninfo mypart \n"); 
		 // return 206;
		 }
		 */
	free(tagbuf);		 
	
 }

 str_alloc_cpy(pbjn_info->filename,source);

 if(get_N1N2N3(fin, &(pbjn_info->n1), &(pbjn_info->n2), &(pbjn_info->n3))) return 207;

 pbjn_info->gridname=NULL; // ��� ��� ����, ��� �� � get_string 
						   // �������� ������ ��� pbjn_info->gridname
 get_string(fin, &(pbjn_info->gridname));

 // ����� ������� ����� �� ��������, �� ���� ���������
 // ��� ����� ����� ������ ������� � ������ ������, �� ������ �� ������, 
 // ������� ���������

 if(err=finde_func_counts_and_names(fin,(char *)"",&pfunc,&plast,&(pbjn_info->n_func),NULL))return err;
 if(pfunc)
	 {
		// ���� ������� � ������ ������, � �� ������ ���� ����
		 return 208;
	 }

// Allocating massives

 pbjn_info->funcNames=(LPLPSTR)
	 alloc_char_mas_n1(pbjn_info->n_func*(sizeof(LPSTR)));

 pbjn_info->fmin=(LPFLOAT)
	 alloc_char_mas_n1(pbjn_info->n_func*(sizeof(pbjn_info->fmin[0])));

 pbjn_info->fmax=(LPFLOAT)
	 alloc_char_mas_n1(pbjn_info->n_func*(sizeof(pbjn_info->fmax[0])));

 pbjn_info->nBitsReduce=(LPINT)
	 alloc_char_mas_n1(pbjn_info->n_func*(sizeof(pbjn_info->nBitsReduce[0])));

// for(i=0;i<pbjn_info->n_func;i++)
//	str_alloc_cpy(pbjn_info->funcNames[i],bjnh.pfh[i].name);

 if(err=finde_func_counts_and_names(fin,(char *)"",&pfunc,&plast,&(pbjn_info->n_func),pbjn_info->funcNames))return err;
 if(pfunc)
	 {
		// ���� ������� � ������ ������, � �� ������ ���� ����
		 return 209;
	 }


// finish of allocating
//----------------------+++++++++++++++++++++++++++++++++++++++++++++++++++++++
 fclose(fin);
//----------------------+++++++++++++++++++++++++++++++++++++++++++++++++++++++
 if( (fin=fopen(source,"rb")) == NULL )
   { printf("\n ReadBjnGzippedScalar8RecInfo: Error: Can't open for read file %s ",source ); return 3; }

 for(i=0;i<pbjn_info->n_func;i++)
	{
		/* �� ��� ������ ����
	err=fseek(fin,bjnh.pfh[i].offset_of_func_blocks,SEEK_SET);

	if(err)
		{
		printf("Error: input of func data blocks: err=%d fseek on %d i=%d\n",
		 err,bjnh.pfh[i].offset_of_func_blocks,i);
		if(err)
		return 54;
		}

	Dprintf("bread fmi/fma: ftell=%d\n",ftell(fin));
		*/

	err=finde_func_counts_and_names(fin, pbjn_info->funcNames[i], &pfunc, &plast, NULL,NULL);

	fseek(fin,pfunc,SEEK_SET);
	if(get_file_offset(fin, &pdata)) return 210; // ��������� �������
	if(get_file_offset(fin, &pdata)) return 211; // ���� ������

	if(get_float(fin, &(pbjn_info->fmin[i]))) return 212;
	if(get_float(fin, &(pbjn_info->fmax[i]))) return 213;

    get_data_block_BIT_REDUCED(fin, pdata, &(pbjn_info->nBitsReduce[i]));

	Dprintf("min/max[%d] : %g %g : %d\n",i,
		pbjn_info->fmin[i],pbjn_info->fmax[i],
		pbjn_info->nBitsReduce[i]
		);
	}

 fclose(fin);

 return 0;
}

void PrnBjnInfo(FILE *fout, LPBJN_INFO pbjn_info)
{
 int i;

// printf("\n PrnBjnInfo \n");

 printf("%s : %d %d %d : grid `%s` : nfunk %d `%s`\n",
		(*(pbjn_info)).filename,
		(*(pbjn_info)).n1,
		(*(pbjn_info)).n2,
		(*(pbjn_info)).n3,
		(*(pbjn_info)).gridname,
		(*(pbjn_info)).n_func,
		(*(pbjn_info)).tagOfBjnFile
	   );

 for(i=0;i<pbjn_info->n_func;i++)
	{
	printf("%2d %s : from %g to %g : %d bits reduced\n",i,
		pbjn_info->funcNames[i],
		pbjn_info->fmin[i],pbjn_info->fmax[i],
		pbjn_info->fmin[i],pbjn_info->fmax[i],
		pbjn_info->nBitsReduce[i]		);
	}

// printf("\nprn_bjnh finish\n");
}

void FreeBjnInfo(LPBJN_INFO pbjn_info)
{
 int i;

 if(!pbjn_info) return;

 if(1) // ToDo !!
 {
 free(pbjn_info->filename);
 free(pbjn_info->gridname);

 //
 
 free(pbjn_info->fmin);
 free(pbjn_info->fmax);
 free(pbjn_info->nBitsReduce);
 //printf("pbjn_info->n_func = %d\n",pbjn_info->n_func);
 for(i=0;i<pbjn_info->n_func;i++)
	free(pbjn_info->funcNames[i]);

 free(pbjn_info->funcNames);
 
 //delete [] pbjn_info->funcNames;
 }

 pbjn_info->n_func=0;
 pbjn_info->n1=0;
 pbjn_info->n2=0;
 pbjn_info->n3=0;

 return;
}
