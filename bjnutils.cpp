#include "bjn.h"
#include "string.h"
/* ===========================================================================
   Outputs a long in LSB order to the given file
*/

file_offset filelen(FILE *f)
{
file_offset len,cur;

cur = ftell(f);
      fseek(f,  0,SEEK_END);
len = ftell(f);
      fseek(f,cur,SEEK_SET);

return len;
}

int put_string(FILE *fout, LPCHAR name)
{
 int bytes=strlen(name);

 if(put_long(fout, bytes)) return 1;

 if(bytes)
	if(fwrite((LPCHAR)name,bytes,1,fout)!=1) return 1;

 return 0;
}

int put_file_offset (FILE *file, file_offset a)
{
	return put_long (file, (long) a);
}

int put_big_size (FILE *file, big_size a)
{
	return put_long (file, (long) a);
}

int put_long (FILE *file, long a)
{
    int n;
	unsigned long x=(unsigned long)a;

    for (n = 0; n < 4; n++) {
        if(fputc((int)(x & 0xff), file)==EOF)return 1;
        x >>= 8;
    }

 return 0;
}

int put_int(FILE *file, int a)
{
    int n;
	unsigned long x=(unsigned long)a;

    for (n = 0; n < 4; n++) {
        if(fputc((int)(x & 0xff), file)==EOF)return 1;
        x >>= 8;
    }

 return 0;
}


int get_string(FILE *fout, LPCHAR *pname)
{
 int bytes;

 *pname=NULL;

 if(get_longL(fout, &bytes)) return 201;

 if(bytes)
	{
	//printf("we have this allocate \n");	
	*pname=alloc_char_mas_n1(bytes+1);
	if(fread((LPCHAR)(*pname),bytes,1,fout)!=1) return 202;
	(*pname)[bytes]=0;
	}

 return 0;
}



int get_long(FILE *fin, uLong *a)
{
 uLong x=0;
 unsigned char c;

#define PC(i) printf("get_long%d %5d %5d\n",i,x,c);
#undef PC
#define PC(i)
 if(get_char(fin,(char *)&c))return 1; PC(0) x  = (uLong)c;
 if(get_char(fin,(char *)&c))return 2; PC(1) x +=((uLong)c)<<8;
 if(get_char(fin,(char *)&c))return 3; PC(2) x +=((uLong)c)<<16;
 if(get_char(fin,(char *)&c))return 4; PC(3) x +=((uLong)c)<<24;
#undef PC

 *a=x;

 return 0;
}

int get_int(FILE *fin, int *a)
{
 uLong x=0;
 unsigned char c;

#define PC(i) printf("get_long%d %5d %5d\n",i,x,c);
#undef PC
#define PC(i)
 if(get_char(fin,(char *)&c))return 1; PC(0) x  = (uLong)c;
 if(get_char(fin,(char *)&c))return 2; PC(1) x +=((uLong)c)<<8;
 if(get_char(fin,(char *)&c))return 3; PC(2) x +=((uLong)c)<<16;
 if(get_char(fin,(char *)&c))return 4; PC(3) x +=((uLong)c)<<24;
#undef PC

 *a=x;

 return 0;
}

int put_char(FILE *file, char a)
{
 if(fputc((int)((unsigned char)a & 0xff), file)==EOF)return 1;

 return 0;
}



int get_char(FILE *fin, LPCHAR c)
{
 if(fread(c,1,1,fin)!=1) return 1;

 return 0;
}


int get_longL(FILE *fout, LPINT pn)                                                                                                 
{                                                                                                                                   
 uLong nn;                                                                                                                           
                                                                                                                                    
 if(get_long(fout, &nn)) return 12;                                                                                       
                                                                                                                                    
 *pn=(int)(nn);                                                                                                                     
                                                                                                                                    
 return 0;                                                                                                                          
}   


int get_big_size(FILE *fout, big_size *pn)                                                                                                 
{                                                                                                                                   
 uLong nn;                                                                                                                           
                                                                                                                                    
 if(get_long(fout, &nn)) return 12;                                                                                       
                                                                                                                                    
 *pn=(int)(nn);                                                                                                                     
                                                                                                                                    
 return 0;                                                                                                                          
}   

int get_file_offset(FILE *fout, file_offset *pn)                                                                                                 
{                                                                                                                                   
 uLong nn;                                                                                                                           
                                                                                                                                    
 if(get_long(fout, &nn)) return 12;                                                                                       
                                                                                                                                    
 *pn=(int)(nn);                                                                                                                     
                                                                                                                                    
 return 0;                                                                                                                          
}   

int get_float(FILE *fin, float *a)
{
return get_longL(fin,(LPINT)a);
}


int put_float (FILE *file, float a)
{
#if 1
 return put_long(file,(long)(*(   (long *)      (&a)    )));
#else
    int n;
	unsigned long x=(unsigned long)(*((unsigned long*)(&a)));

    for (n = 0; n < 4; n++) {
        if(fputc((int)(x & 0xff), file)==EOF)return 1;
        x >>= 8;
    }

 return 0;
#endif
}

int IsSegCrossed(int a1, int a2, int b1, int b2)
{
 return !( (a2<b1) || (b2<a1));
}

int IsBlocksCrossed( int i0, int j0, int k0, int ni, int nj, int nk,
					 int i0g,int j0g,int k0g,int ing,int jng,int kng)
{
 return
	 IsSegCrossed(i0,i0+ni-1,i0g,i0g+ing-1) &&
	 IsSegCrossed(j0,j0+nj-1,j0g,j0g+jng-1) &&
	 IsSegCrossed(k0,k0+nk-1,k0g,k0g+kng-1);
}



