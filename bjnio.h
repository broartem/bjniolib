#ifndef _BJNIO_H_
#define _BJNIO_H_

/*TODO ����������� �������� LLABEL */
#define LLABEL 16

typedef char      * LPCHAR;
typedef int       * LPINT;
typedef float     * LPFLOAT;
typedef void      * LPVOID;

typedef LPCHAR    * LPLPCHAR;
typedef LPFLOAT   * LPLPFLOAT;
typedef LPINT     * LPLPINT;
typedef LPLPINT   * LPLPLPINT;
typedef LPLPFLOAT * LPLPLPFLOAT;
typedef LPVOID    * LPLPVOID;
typedef LPLPVOID  * LPLPLPVOID;

LPCHAR		alloc_char_mas_n1(int n1);
LPINT		alloc_int_mas_n1(int n1);
LPFLOAT		alloc_float_mas_n1(int n1);

LPLPFLOAT	alloc_float_mas_n1_n2(int n1, int n2);
LPLPLPFLOAT alloc_float_mas_n1_n2_n3(int n1, int n2, int n3);
LPLPLPINT   alloc_int_mas_n1_n2_n3(int n1, int n2, int n3);

void free_mas_n1_n2(LPLPVOID *p,int n1,int n2);
void free_mas_n1_n2_n3(LPLPLPVOID *p,int n1,int n2,int n3);

typedef unsigned long big_size; 

typedef struct
	{
	int		 n1,n2,n3;
	int		 n_func;
	LPCHAR	 filename;
	LPCHAR	 gridname;
	LPLPCHAR funcNames;
	LPFLOAT	 fmin,fmax;
	LPINT	 nBitsReduce;
	char	 tagOfBjnFile[LLABEL+1];
	}
		BJN_INFO;

typedef BJN_INFO *LPBJN_INFO;


#ifdef __cplusplus
extern "C" {
#endif

int BjnPutErrMess(int num);

/* WriteBjnGzippedScalar8RecInit c������� ����� ��� ������ �������. */
int WriteBjnGzippedScalar8RecInit(
								  LPCHAR target, // ��� �����
								  LPCHAR grid_name, // ��� ����� ����� ��� ""
								  big_size n1, big_size n2, big_size n3 // ������� ���������� �����
								  );

int	WriteBjnGzippedScalar8RecFuncMinMax(
								  LPCHAR target,
								  LPCHAR funcName, 
								  float fmin, 
								  float fmax);

/* WriteBjnGzippedScalar8RecFunc ������ � ���� ��������� ������� */
int WriteBjnGzippedScalar8RecFunc(
								  LPCHAR target, // ��� �����
								  LPCHAR funcName, // ��� �������
								  LPLPLPFLOAT f, // ���������� ������ ������������ ������� 
								  big_size n1, big_size n2, big_size n3, // ������� ������� f
								  int nBitsReduce // ����� �� ����������� ��� ��������
                  );
/* TODO ���������� �������� ���������� ������� WriteBjnGzippedScalar8RecFuncByBlock */
int	WriteBjnGzippedScalar8RecFuncByBlock(
								  LPCHAR target,
								  LPCHAR funcName, 
								  LPLPLPFLOAT f,
								  big_size i0, big_size j0, big_size k0,
								  big_size in, big_size jn, big_size kn,
								  int nBitsReduce);

/* ������ ���������� ����� */
//int  ReadBjnGzippedScalar8RecInfo(LPCHAR source, LPBJN_INFO pbjn_info);

/* ReadBjnGzippedScalar8RecFunc ������ ����������� ������� ������� */
int  ReadBjnGzippedScalar8RecFunc(LPCHAR source, // ��� �����
								  LPCHAR funcName, // ��� �������
								  LPLPLPFLOAT *pf,  // ������ �� ���������� ������ ������� f
								  LPINT pn1,LPINT pn2,LPINT pn3 // ������ �� ������� ������� f
                  );

/* TODO ���������� �������� ���������� ������� ReadBjnGzippedScalar8RecFuncByBlock */
int	 ReadBjnGzippedScalar8RecFuncByBlock(
								  LPCHAR source, 
								  LPCHAR funcName, 
								  LPLPLPFLOAT *pf, // ������ �� ���������� ������ ������� f
								  big_size i0g, big_size j0g, big_size k0g,
								  big_size ing, big_size jng, big_size kng);

/* TODO ���������� �������� ���������� ������� PrnBjnInfo */
//int PrnBjnInfo(FILE *fout, LPBJN_INFO pbjn_info);

int  ReadBjnGzippedScalar8RecInfo(LPCHAR source, LPBJN_INFO pbjn_info);

/* TODO ���������� �������� ���������� ������� FreeBjnInfo */
void FreeBjnInfo(LPBJN_INFO pbjn_info);

int	WriteBjnGzippedScalar8RecFuncByBlockD(
								  LPCHAR target,
								  LPCHAR funcName, 
								  double *f,
								  big_size i0, big_size j0, big_size k0,
								  big_size in, big_size jn, big_size kn,
								  int nBitsReduce);

int	 ReadBjnGzippedScalar8RecFuncByBlockD(
								  LPCHAR source, 
								  LPCHAR funcName, 
								  double *f, // ���������� ������ ������� f
								  big_size i0g, big_size j0g, big_size k0g,
								  big_size ing, big_size jng, big_size kng);

#define minab(a,b)     (((a) > (b)) ? (b) : (a))
#define maxab(a,b)     (((a) > (b)) ? (a) : (b))

#ifdef __cplusplus
}
#endif

#endif /* _BJNIO_H_ */
