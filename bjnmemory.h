#ifndef _BJNMEMORY_H_
#define _BJNMEMORY_H_

#pragma warning (push)
#pragma warning (disable:279)

#include "bjn.h"

#ifdef __cplusplus
extern "C" {
#endif

#define _fmalloc malloc
#define  halloc(a,b) malloc((a)*(b))
#define _ffree free
#define  hfree free

static LPCHAR ErrAllocMsg =(char *)" \n not enough room to allocate memory\n";
static LPCHAR ErrAllocMsg1=(char *)" \n data segment > 64K \n";
static LPCHAR ErrAllocMsg2=(char *)" \n pointer segment > 64K \n";

//LPCHAR   alloc_char_mas_n1    (int n1);
//LPINT    alloc_int_mas_n1     (int n1);
//LPFLOAT  alloc_float_mas_n1   (int n1);
//LPDOUBLE alloc_double_mas_n1  (int n1);

LPLPCHAR   alloc_char_mas_n1_n2 (int n1, int n2);
LPLPINT    alloc_int_mas_n1_n2  (int n1, int n2);
//LPLPFLOAT  alloc_float_mas_n1_n2(int n1, int n2);
//LPLPDOUBLE alloc_double_mas_n1_n2(int n1, int n2);

//LPLPLPCHAR   alloc_char_mas_n1_n2_n3(int n1, int n2, int n3);
//LPLPLPINT    alloc_int_mas_n1_n2_n3(int n1, int n2, int n3);
//LPLPLPFLOAT  alloc_float_mas_n1_n2_n3(int n1, int n2, int n3);
//LPLPLPDOUBLE alloc_double_mas_n1_n2_n3(int n1, int n2, int n3);


#define str_alloc_cpy(t,s)				\
{									\
	if(s)								\
		{		\
		t=alloc_char_mas_n1(strlen(s)+1);	\
		strcpy(t,s);						\
		}		\
	else		\
		t=NULL;	\
}

#ifdef __cplusplus
}
#endif

#pragma warning (pop)
#endif /* _BJNMEMORY_H_ */
