#include "bjn.h"

#pragma warning (disable:181)

int get_block_of_float_mas(FILE *fout, LPLPLPFLOAT f, 
						   int i0g, int j0g, int k0g, 
						   int n1,int n2,int n3, 
						   int *pnBitsReduce,
						   int i0, int j0, int k0, 
						   int ni, int nj, int nk)
{
 int N1;
 uLong size_compressed;
 LPCHAR	buf1,buf2,pbufs,pbuft;
 LPFLOAT buf0;
 int	i,j,k,err;
 int	n,ns,nt;
 char ci;

 if(get_char(fout,&ci))return 157;

 Dprintf("Cycles of compression=%d\n",ci);

 N1=(ni*nj*nk*sizeof(float));

 if(!N1)return 0;

 buf0=alloc_float_mas_n1(ni*nj*nk); if(!buf0) return 197;
 buf1=alloc_char_mas_n1(N1); if(!buf0) return 198;

 if(get_long(fout,&size_compressed)) return 158; // ?????

 buf2=alloc_char_mas_n1(maxab(N1,(int)size_compressed)); if(!buf2) return 159;
 if(fread((LPCHAR)buf2,size_compressed,1,fout)!=1) return 160;

 pbuft=buf1;
 pbufs=buf2;	
 ns=size_compressed;

 for(i=0;i<ci;i++)
	{
	nt=N1;
	{	
	uLong nnt=(uLong)nt;
//	{int k;printf("\nns=%d\n",ns);for(k=0;k<ns;k++)printf("%3d %3u\n",k,(unsigned int)pbufs[k]);} // !!!!!!!!!!
	err= uncompress ((Bytef *)pbuft, &nnt, (Bytef *)pbufs, ns); if(err) return err+162000;//  (buf1,&nt, buf2 ,N1);
	//err=10101;
	nt=(int)nnt;
	}
	
	if(err)
		{
		printf("get_block_of_float_mas: uncompress: ns=%d -> nt=%d(%d) err=%d : %s \n",ns,nt,size_compressed,err,zError(err));
		return 161;
		}

	{LPCHAR p; p=pbufs; pbufs=pbuft; pbuft=p;}
	ns=nt;
	}

 if(ns!=N1)
	{
	 printf("get_block_of_float_mas: Error before unrebyte : %d != %d\n",ns,N1);
	 return 163;
	}

 n=ns;
 err=unrebyte_float_mas((LPCHAR)buf0,n,(LPCHAR)pbufs,ns);

 if(err)
	{
	printf("put_block_of_float_mas: rebyte: N1=%d -> n=%d(%d) err=%d\n",N1,n,N1,err);
	return err+164000;
	}

 if(!f)
	{
	 printf("Unallocated input massive\n");
	 return 165;
	}

// TODO ������������� !!!!!!!!!

 for(n=0,i=0;(i<ni);i++)
   for(  j=0;(j<nj);j++)
     for(k=0;(k<nk);k++) // ���������� ������� �����
	 {
		 int ii=i+i0-i0g;
		 int jj=j+j0-j0g;
		 int kk=k+k0-k0g;

		 if((ii>=0)&&(jj>=0)&&(kk>=0)&&(ii<n1)&&(jj<n2)&&(kk<n3))
			f[ii][jj][kk]=buf0[n];
		 n++;
	 }


 free(buf0);
 free(buf1);
 free(buf2);

 return 0;
}

int put_block_of_float_mas(FILE *fout, LPLPLPFLOAT f, int nBitsReduce,
						   int i0, int j0, int k0, int ni, int nj, int nk)
{
 int N1,N2;
 LPCHAR	buf1,buf2,pbufs,pbuft;
 LPFLOAT buf0;
 int	i,j,k,err;
 uLong	n,ns,nt,bits_mask;

 N1=(ni*nj*nk*sizeof(float));
 N2=(N1*12/10+12);

 buf0=alloc_float_mas_n1(ni*nj*nk);
 buf1=alloc_char_mas_n1(N2);
 buf2=alloc_char_mas_n1(N2);

 bits_mask=set_bits_mask(nBitsReduce);

#define Float(a) ((float)(*((float *)(&(a)))))
#define ULong(a) ((uLong)(*((uLong *)(&(a)))))

 for(n=0,i=0;i<ni;i++)
   for(j=0;j<nj;j++)
     for(k=0;k<nk;k++)
		{
		uLong tmp=ULong(f[i][j][k]) & bits_mask;
		buf0[n++]=Float(tmp);
		}

 n=N1;

 err=rebyte_float_mas(buf1,n,(LPCHAR)buf0,N1);

 if(err)
	{
	printf("put_block_of_float_mas: rebyte: <%d:%d> N1=%d -> n=%lu(%d) err=%d\n",i,j,N1,n,N1,err);
	return 199;
	}

// ------------------

 pbufs=buf1; ns=n;
 pbuft=buf2; nt=N2;

 // TODO !!!!! ������� ��������������� �������� �� 2008.03.09
 for(i=0;i<2;i++)
	{
	
		err= compress ((Bytef *)pbuft, &nt,  (Bytef *)pbufs ,ns); // <-
		//err=10102;

	if(err)
		{
		printf("put_block_of_float_mas: compress: <%d:%d> N1=%d -> n=%d(%d) err=%d : %s \n",i,j,N1,n,N2,err,zError(err));
		return 195;
		}

	Dprintf("Compression: %d %10d -> %10d\n",i,ns,nt);
//	if((i<1)&&(nt<ns))
	if(nt<ns)
		{		// let continue compression
		ns=nt;
		nt=N2;

		{LPCHAR p; p=pbufs; pbufs=pbuft; pbuft=p;}
		}
	else
		break;
	}
// ------------------

 Dprintf("Cycles of compression=%d\n",i);

 put_char(fout,(char)i);

// iak 080306
// if(fwrite((LPCHAR)"<<<<",4,1,fout)!=1) return 3; // buf2 !!
// if(fwrite((LPCHAR)&ns,sizeof(ns),1,fout)!=1) return 3; // buf2 !!
// if(fwrite((LPCHAR)"----",4,1,fout)!=1) return 3; // buf2 !!

//{uLong k;printf("\nwrite ns=%d\n",ns);for(k=0;k<ns;k++)printf("%3d %3u\n",k,(unsigned int)pbufs[k]);} // !!!!!!!!!!

 put_long(fout,ns);
 if(fwrite((LPCHAR)pbufs,ns,1,fout)!=1) return 196; // buf2 !!

// if(fwrite((LPCHAR)">>>>",4,1,fout)!=1) return 3; // buf2 !!

 free(buf0);
 free(buf1);
 free(buf2);

 return 0;
}



int rebyte_float_mas(LPCHAR tar, int n_tar, LPCHAR sou, int n)
{
 int i,j,m=n/sizeof(float);
 unsigned char s,t;

 if(n_tar!=n)
		return 1;

 for(i=0;i<n;i+=sizeof(float))
	 for(j=0;j<sizeof(float);j++)
		{
		s=sou[i+j];
		switch(j)
			{
			case 0: t=		0; break;
			case 1: t= s&0xf0; break;
			case 2: t= s	 ; break;
			case 3: t= s	 ; break;
			}

		 tar[j*m+i/sizeof(float)]=s;//t;

//		 			(j<0?0:sou[i+j]); //only for really float
		}

 return 0;
}


uLong set_bits_mask(int nBitsReduce)
{
 uLong a;
 int i;

 for(a=1,i=0;i<32-nBitsReduce;i++)
	 a=a<<1|1;

 for(;i<32;i++)
	 a=a<<1;

 return a;
}

int unrebyte_float_mas(LPCHAR tar, int n_tar, LPCHAR sou, int n)
{
 int i,j,m=n/sizeof(float);

 if(n_tar!=n)
		return 1;

 for(i=0;i<n;i+=sizeof(float))
	 for(j=0;j<sizeof(float);j++)
		 tar[i+j]=sou[j*m+i/sizeof(float)];

 return 0;
}

