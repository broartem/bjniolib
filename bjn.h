#ifndef _BJN_H_
#define  _BJN_H_

/* File handling routines */
#include "bjnio.h"
#include "string.h"

#define  BjnDummy 0
#define  BjnFunc (i)

#define _CRT_SECURE_NO_WARNINGS

#ifdef __cplusplus
  #include <cstdio>
  #include <cerrno>
  #include <ctype.h>
  #include <cstdlib>
  #include <string>
  #include <ctime>
#else
  #include <stdio.h>
  #include <errno.h>
  #include <ctype.h>
  #include <stdlib.h>
  #include <string.h>
  #include <time.h>
  #include <string.h>
#endif

/* Zlib header files */
//#include <zconf.h>
#include "zlib.h"


/* Basic types redefenitions  */
#include "bjntypes.h"

/* 1D, 2D, 3D Memory allocation routines */
#include "bjnmemory.h"
#include "bjndebug.h"
#include "bjnutils.h"

/* Macros of general use  */
#define MulDiv(a,b,c)  (int)( ((long)(a)) *(long)(b) / (long)(c) )
#define qva(a)         ((a) * (a))




/* TODO: Deprecated */

#define Copyright(name,x,y,z) \
static char *name = "\r\n" #x " - Copyright (C) M.Iakobovski " #y " Version " #z " ";

#define ErrMsg printf

/* TODO: End of deprecated */


#endif  /* _BJN_H_ */

